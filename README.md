# Signal Sticker Packs

This repo is a collection of sticker packs. Eventually, it will render all known
packs to a GitLab Pages website. To add a pack, update `sticker-packs.yaml` and
submit an MR.
